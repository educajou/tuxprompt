# Tuxprompt

Tuxprompt est une application de type [prompteur](https://fr.wikipedia.org/wiki/Prompteur#).

## Utilisation

Se rendre sur l'instance https://educajou.forge.apps.education.fr/tuxprompt/

- Écrire ou coller un texte
- Choisir la taille de police
- Choisir la vitesse de lecture en nombre de mots /minue
- Cliquer sur le bouton "C'est parti".

## Fonctionnalités

- mode sombre / mode clair
- défilement horizontal ou vertical

## Licence

Tuxprompt est une application libre sous licence GNU / GPL.

