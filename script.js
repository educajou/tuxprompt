// 2024 Arnaud Champollion //
// Tuxprompt //
// Licence GNU GPL //

// Constituants de la page
const divConteneurTexte = document.getElementById('zone-texte');
const panneau = document.getElementById('panneau');
const divOutils = document.getElementById('outils');
const boutonPanneau = document.getElementById('bouton-panneau');
const boutonClairsombre = document.getElementById('bouton-clair-sombre');
const boutonScroll = document.getElementById('bouton-scroll');
const popup = document.getElementById('popup');
const boutonCopierLien = document.getElementById('bouton-copier-lien');
const boutonQrCode = document.getElementById('bouton-qrcode');

const divQrcode=document.getElementById('div-qrcode');
const divImport=document.getElementById('div-import');
const divBlur=document.getElementById('blur');
const zoneQrcode=document.getElementById('zone-qrcode');
const boutonPlay = document.getElementById('bouton-play');
const inputVitesseLecture = document.getElementById('input-vitesse-lecture');
const inputOpacite = document.getElementById('input-opacite');
const inputImport = document.getElementById('input-import');
const darkbox = document.getElementById('darkbox');
const divApropos = document.getElementById('apropos');
const educajou = document.getElementById('educajou');
const zoneSaisie = document.getElementById('zone-saisie');
const entreeTexte = document.getElementById('entree-texte');
const boutonCommencer = document.getElementById('bouton-commencer');
const texteDefilant = document.getElementById('texte');
const loader = document.getElementById('loader');
const inputLargeur = document.getElementById('reglage-largeur');

const tempsLecture = document.getElementById('temps-lecture');
const boutonsLire = document.querySelectorAll('.lire');
const boutonsEcrire = document.querySelectorAll('.ecrire');

const body = document.body;

const root = document.documentElement;

const mentionFramaCodim = " (lien CodiMD ou Framapad) ";


// Ce préfixe sera utilisé pour le stockage des paramètres dans le cache
let prefixeAppli = 'tuxprompt';

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);

// Adaptations de l'interface pour Openboard
if (openboard){
    document.body.classList.add('openboard');
    educajou.href='https://educajou.forge.apps.education.fr/';
}

// Variables globales
let texte = null;
let lectureEnCours = false;
let taillePolice = 48;
let modeEcrire = true;
let vitesseLecture = 90;
let intervalle = 50;
let modeCouleur = "light";
let modeDefilement = "vertical";
let panneauVisible = false;
let framacodimd = false;

// Réglages de l'interface d'après les variables
inputVitesseLecture.value = vitesseLecture;
entreeTexte.style.fontSize = taillePolice + 'px';
inputLargeur.value = 100;
divConteneurTexte.style.setProperty('--after-width', 'calc(100% - 100px');
inputOpacite.value = 75;

changeOpacite(75);


// Chargement du thème sombre ou clair
if (
	window.matchMedia &&
	window.matchMedia("(prefers-color-scheme: dark)").matches
) {
	changeClairSombre();
}


// Désactivation de tous les boutons inutiles en mode saisie
boutonsLire.forEach(function(bouton) {
    bouton.disabled = true;
});



// Vérification des paramètres sont dans l'URL
function checkUrl() {
    console.log('Lecture URL');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);

    // Primtux
    if (urlParams.get('primtuxmenu')==="true") {
        educajou.style.display='none';
    }

    if (urlParams.get('vitesse-lecture')) {
        vitesseLecture = parseInt(urlParams.get('vitesse-lecture'));
    }
}

function appliqueReglages() {
    console.log('Application des réglages');
    inputVitesseLecture.value = vitesseLecture;  
}

async function verifieStockageLocal() {
    console.log('Lecture stockage local');

    const opacite = await litDepuisStockage('opacite');
    if (opacite){
        inputOpacite.value = opacite;
        changeOpacite(opacite);
    }
}

///////////////////// Lancement du programme ///////////////////
async function executeFunctions() {
    await verifieStockageLocal();
    checkUrl();
    appliqueReglages();
    verifieLien();    
}
executeFunctions();
//////////////////////////////////////////////////////////////

// Obtenir l'URL actuelle
const urlComplete = window.location.href;
// Fonction pour extraire l'URL jusqu'au dièse
function obtenirUrlSansHash(url) {
    const indexHash = url.indexOf('#');
    if (indexHash !== -1) {
        return url.substring(0, indexHash);
    }
    return url; // Si aucun dièse n'est trouvé, renvoyer l'URL entière
}
let debutLien = obtenirUrlSansHash(urlComplete);
let lien = debutLien;

//Paramètres du QR-code
let qrcode = new QRCodeStyling({
    width: 800,
    height: 800,
    data: debutLien, // Le lien ou le texte que vous souhaitez encoder
    image: "images/favicon.svg",
    dotsOptions: {
        color: "#000", // Couleur des points du QR code
        type: "round" // Type de points (square, round, etc.)
    },
    backgroundOptions: {
        color: "#fff" // Couleur de fond du QR code
    }
});


// Ajout du QR code
qrcode.append(zoneQrcode);
qrCodeElement = document.querySelector('#zone-qrcode canvas');

function verifieLien(lienManuel){
    console.log('Vérification du lien '+lienManuel)

// Si un exercice est dans l'URL lancer directement le prompteur
    if (window.location.hash || lienManuel) {

        let chaine_a_examiner;
        
        if (lienManuel){
            chaine_a_examiner = lienManuel;
        } else {
            chaine_a_examiner = decodeURIComponent(window.location.hash.substring(1));
            if (chaine_a_examiner.endsWith('/')) {
                chaine_a_examiner = chaine_a_examiner.slice(0, -1);
            }
        }

        console.log('chaine à examiner ' + chaine_a_examiner);

        
        if (chaine_a_examiner.startsWith('http')) { // Si http après le hash, on va chercher la phrase dans un fichier MD
            let url = handleURL(chaine_a_examiner);
            fetchTextFromURL(url).then(texte => {
                framacodimd = true;
                body.classList.add('framacodim');
                boutonCopierLien.title += mentionFramaCodim;
                boutonQrCode.title += mentionFramaCodim;
                lien = debutLien + '#' + chaine_a_examiner;
                console.log('Nouveau lien : ',lien)
                majUrl(lien);
                entreeTexte.innerHTML = texte;
                afficherTexte(texte);                
            }).catch(err => {
                console.error('Erreur lors de la récupération du texte:', err);
            });
        } else { // Sinon on utilise le contenu après le # comme texte        
            let texte;
            if (chaine_a_examiner.endsWith('/')) { // Vérifier si le dernier caractère est '/'
                // Supprimer le dernier caractère
                texte = chaine_a_examiner.slice(0, -1);
                console.log(texte);
            } else {
                // Sinon, garder la chaîne telle quelle
                texte = chaine_a_examiner;
            }

            entreeTexte.innerHTML = texte;

            afficherTexte(texte);
        }
        
    }

}




function importe(lienManuel) {
    console.log('import du lien '+lienManuel)
    verifieLien(lienManuel);
}

function handleURL(url) {
    if (url !== "") {
        // Gestion des fichiers hébergés sur github
        if (url.startsWith("https://github.com")) {
            url = url.replace(
                "https://github.com",
                "https://raw.githubusercontent.com"
            );
            url = url.replace("/blob/", "/");
        }
        // gestion des fichiers hébergés sur codiMD / hedgedoc / digipage
        if (
            (url.startsWith("https://codimd") || url.includes("hedgedoc") || url.includes("digipage") )
        ) {
            url = url.replace("?edit", "").replace("?both", "").replace("?view", "").replace(/#$/,"").replace(/\/$/,'');
            url = url.indexOf("download") === -1 ? url + "/download" : url;
        }
        // gestion des fichiers hébergés sur framapad
        if (url.includes('framapad') && !url.endsWith('/export/txt')) {
            url = url.replace(/\?.*/,'') + '/export/txt';
        }
    }
    return url;
}

async function fetchTextFromURL(url) {
    try {
        let response = await fetch(url);
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        let texte = await response.text();
        return texte;
    } catch (error) {
        console.error('There has been a problem with your fetch operation:', error);
        throw error;
    }
}




// Récupération d'un texte depuis un fichier MarkDown en ligne
function getMarkdownContent(urlMD) {
    console.log('getMarkdownContent ' + urlMD)
    // Récupération du markdown externe
    if (urlMD !== "") {
        if (urlMD.endsWith('/')) {
            // Supprime le dernier caractère / si présent
            urlMD = urlMD.slice(0, -1);
        }
        // Gestion des fichiers hébergés sur github
        if (urlMD.startsWith("https://github.com")) {
            urlMD = urlMD.replace(
                "https://github.com",
                "https://raw.githubusercontent.com"
            );
            urlMD = urlMD.replace("/blob/", "/");
        }
        // Gestion des fichiers hébergés sur codiMD
        if (
            urlMD.startsWith("https://codimd") &&
            urlMD.indexOf("download") === -1
        ) {
            urlMD =
                urlMD.replace("?edit", "").replace("?both", "").replace("?view", "").replace("#", "") +
                "/download";
        }
        // Récupération du contenu du fichier
        return fetch(urlMD)
            .then((response) => response.text())
            .then((texte) => creerTexte(texte))
            .catch((error) => {
                alert("Il y a une erreur dans l'URL. Merci de la vérifier et de vous assurer que le fichier est bien accessible.")
                console.log(error);
                return null; // Retourne null en cas d'erreur
            });
    } else {
        return Promise.resolve(creerTexte(defaultData));
    }
}

// Mise à jour du lien
function majLien(){
    if (texte !== ''){
        console.log('majLien texte = '+texte);
        
        // Encode le texte de l'ancre
        let finLien = encodeURIComponent(texte);
        
        // Construit le lien avec l'ancre encodée
        lien = debutLien + '#' + finLien + '/';
        
        // Met à jour l'URL
        // majUrl(lien);
        
        console.log("Mise à jour du lien : "+lien);
    }
}


// Fonction "télécharger le qRcode"
function telechargerQrcode() {
    // Créer un élément de lien temporaire
    let link = document.createElement('a');

    // Conversion du QR code en URL de données (data URL)
    link.href = qrCodeElement.toDataURL();

    // Nom du fichier à télécharger
    let nomDuFichier='qrcode.png';
    link.download = nomDuFichier;

    // Clic virtuel sur le lien pour déclencher le téléchargement
    link.click();
}

// Copier le lien dans le presse-papiers
function copieLien(){
    navigator.clipboard.writeText(lien)
        .then(() => {
            // Afficher le popup
            const rect = boutonCopierLien.getBoundingClientRect();
            popup.style.left = `${rect.left}px`;
            popup.style.top = '60px';
            popup.style.display = 'block';

            // Cacher le popup après 1 seconde
            setTimeout(() => {
                popup.style.display = 'none';
            }, 1000);
        })
        .catch(err => {
            console.error('Erreur lors de la copie du lien : ', err);
        });
}

function qrCode() {
    loader.classList.remove('hide');
    try {
        qrcode.update({ data: lien });
        qrCodeElement = document.querySelector('#zone-qrcode canvas');
        
        // Check if the QR code canvas element is generated successfully
        if (qrCodeElement) {
            divQrcode.classList.remove('hide');
            divBlur.classList.remove('hide');
        } else {
            throw new Error('QR code generation failed');
        }
    } catch (error) {
        alert('Texte trop long pour générer un code QR');
    } finally {
        // Hide the loader after a short delay
        setTimeout(function() {
            loader.classList.add('hide');
        }, 100); // Adjust the delay as needed
    }
}




// Créer le texte
function creerTexte(content) {
    console.log('fonction creation texte ' + content);
    // Prévoir des filtres pour personnaliser ce qu'on récupère depuis le Markdown.
    // Pour l'instant on prend tout.
    return content;
}



// Appliquer une couleur au texte sélectionné
function applyColor(couleur) {
    console.log('Couleur sélectionnée : ' + couleur);

    // Récupérer le texte sélectionné
    let texteSelectionne = window.getSelection().toString();
    console.log('Texte sélectionné : ' + texteSelectionne);

    if (texteSelectionne) {
        // Appliquer la couleur au texte sélectionné en entourant avec un span
        let spanStyled = '<span style="color: ' + couleur + ';">' + texteSelectionne + '</span>';
        console.log('Style du span : ' + spanStyled);
        // Remplacer le texte sélectionné par le texte entouré du span
        document.execCommand('insertHTML', false, spanStyled);
    }
}




// Fonctionnement du panneau latéral
function visibilitePanneau() {
    if (panneauVisible){
        panneau.style.right=null;
        boutonPanneau.style.right=null;     
    } else {
        panneau.style.right='0px';
        boutonPanneau.style.right='calc(100% + 5px)';
    }
    panneauVisible=!panneauVisible;
}

function clic(event) {
    let cible = event.target;

    // Empêcher la fermeture du panneau si un bouton de couleur est cliqué
    if (cible.classList.contains('color-button')) {
        event.stopPropagation();
        event.preventDefault();
    }

    // Fermer le panneau s'il est ouvert
    if (panneauVisible && !objetOuEnfantDe(cible, panneau) && cible != boutonPanneau && !cible.classList.contains('bouton-options')) {
        visibilitePanneau();
    }
}

// Évènements tactiles
document.addEventListener('touchmove', function(event) {
    if (event.touches.length === 1) {
        var target = event.target;
        var isInsideHaut = false;
        var isInsideConteneur = false;

        // Vérifie si l'élément touché est à l'intérieur de la div avec l'ID 'haut'
        while (target !== null) {
            if (target.id === 'haut') {
                isInsideHaut = true;
                break;
            }
            if (target.id === 'conteneur') {
                isInsideConteneur = true;
                break;
            }
            target = target.parentElement;
        }

        if (!isInsideHaut && !isInsideConteneur) {
            event.preventDefault();
        } else if (isInsideConteneur) {
            // Permet le défilement selon le mode courant
            var touch = event.touches[0];
            var startX = touch.pageX;
            var startY = touch.pageY;

            var handleTouchMove = function(event) {
                var touch = event.touches[0];
                var deltaX = touch.pageX - startX;
                var deltaY = touch.pageY - startY;

                if (modeDefilement === "horizontal" && Math.abs(deltaX) < Math.abs(deltaY)) {
                    event.preventDefault(); // Empêche le scroll vertical
                } else if (modeDefilement === "vertical" && Math.abs(deltaY) < Math.abs(deltaX)) {
                    event.preventDefault(); // Empêche le scroll horizontal
                }
            };

            target.addEventListener('touchmove', handleTouchMove, { passive: false });

            // Nettoie l'événement 'touchmove' après avoir terminé le défilement
            target.addEventListener('touchend', function() {
                target.removeEventListener('touchmove', handleTouchMove);
            }, { once: true });
        }
    }
}, { passive: false });



// Évènements souris
document.addEventListener("mousedown", clic);


// Début
function debut(){
    console.log('Début du programme');
}

// Nouveau
function nouveau() {
    // Boîte de confirmation
    const userConfirmed = confirm("Voulez-vous vraiment effacer le texte ?");

    if (userConfirmed) {
        console.log('nouveau');
        framacodimd = false;
        body.classList.remove('framacodim');
        boutonCopierLien.title = boutonCopierLien.title.replace(mentionFramaCodim, "");
        boutonQrCode.title = boutonQrCode.title.replace(mentionFramaCodim, "");        
        lien = debutLien;
        majUrl(lien);
        entreeTexte.innerHTML='';
        ecrire();
    } else {
        // Si l'utilisateur annule
        console.log("Commande annulée.");
    }
}




// Écrire
function ecrire(){
    divConteneurTexte.classList.add('hide');
    zoneSaisie.classList.remove('hide');
    tempsLecture.classList.add('hide');
    modeEcrire=true;
    boutonsLire.forEach(function(bouton) {
        bouton.disabled = true;
    });
    boutonsEcrire.forEach(function(bouton) {
        bouton.disabled = false;
    });
}

// Créer le texte à afficher dans le prompteur
function formateTexte(texte) {
    if (modeDefilement === 'vertical') {
        return texte;
    } else {
        // Remplacer les balises <div>, <p> pour les rendre inline et ajouter un espace après chacune
        let texteFormate = texte
            .replace(/<div>/g, '<div style="display:inline"> ')
            .replace(/<\/div>/g, '</div> ')
            .replace(/<p>/g, '<p style="display:inline"> ')
            .replace(/<\/p>/g, '</p> ')
            .replace(/<br\s*\/?>/g, ' '); // Remplacer <br> par un espace

        // Supprimer les sauts de ligne
        texteFormate = texteFormate.replace(/[\r\n]+/g, '');
        
        return texteFormate;
    }
}




function nettoieTexte(texteSaisi) {
    return new Promise((resolve, reject) => {
        try {            


// Supprimer tous les attributs sauf 'color', 'font-weight', 'text-decoration', 'background-color' pour les balises <font>
let texteNettoye = texteSaisi.replace(/<font([^>]*)>/gi, function(match, p1) {
    let filteredAttrs = p1.replace(/(\w+)\s*=\s*("[^"]*"|'[^']*'|[^>\s]+)/gi, function(attrMatch, attrName, attrValue) {
        if (attrName.toLowerCase() === 'color') {
            return attrMatch;
        }
        return '';
    });
    return '<font' + filteredAttrs + '>';
});

// Supprimer tous les attributs sauf 'color', 'font-weight', 'text-decoration', 'background-color' pour les autres balises
texteNettoye = texteNettoye.replace(/<(?!font)(\w+)([^>]*)>/gi, function(match, tagName, attrs) {
    if (attrs.toLowerCase().includes('style')) {
        let styleMatch = attrs.match(/style="([^"]*)"/i);
        if (styleMatch) {
            let filteredStyles = styleMatch[1].split(';').map(style => style.trim().split(':').map(prop => prop.trim())).filter(([prop]) => ['color', 'font-weight', 'text-decoration', 'background-color'].includes(prop.toLowerCase()));
            let styleStr = filteredStyles.map(prop => prop.join(':')).join('; ');
            return '<' + tagName + (styleStr ? ' style="' + styleStr + '"' : '') + '>';
        }
    }
    return '<' + tagName + '>';
});

// Liste des balises de type bloc à remplacer
const blockTags = ['div', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ul', 'ol', 'li', 'blockquote', 'pre', 'table', 'tr', 'td', 'th', 'thead', 'tbody', 'tfoot'];

// Construire des expressions régulières pour les balises d'ouverture et de fermeture
const openingTagsRegex = new RegExp(`<(${blockTags.join('|')})[^>]*>`, 'gi');
const closingTagsRegex = new RegExp(`</(${blockTags.join('|')})>`, 'gi');

// Remplacer les balises d'ouverture par une chaîne vide
texteNettoye = texteNettoye.replace(openingTagsRegex, '');

// Remplacer les balises de fermeture par <br>
texteNettoye = texteNettoye.replace(closingTagsRegex, '<br>');

// Nettoyer les <br> multiples éventuels
texteNettoye = texteNettoye.replace(/(<br>\s*)+/gi, '<br>');




            

            resolve(texteNettoye);
        } catch (error) {
            reject(error);
        }
    });
}



async function afficherTexte(texteSaisi) {
    console.log('Texte saisi = ' + texteSaisi);
    if (texteSaisi.length > 0) {
        try {
            const texteNettoye = await nettoieTexte(texteSaisi);
            texte = texteNettoye;
            console.log('Texte nettoyé (2) = ' + texteNettoye);
            entreeTexte.innerHTML = texteNettoye;
            if (!framacodimd){
                majLien();
            }
            // On cache la zone de saisie.
            zoneSaisie.classList.add('hide');
            tempsLecture.classList.remove('hide');

            let texteAAficher = formateTexte(texteNettoye);
            texteDefilant.innerHTML = texteAAficher;

            // On affiche cette zone.
            divConteneurTexte.classList.remove('hide');
            divConteneurTexte.scrollTop = 0;
            divConteneurTexte.scrollLeft = 0;

            boutonsLire.forEach(function (bouton) {
                bouton.disabled = false;
            });
            boutonsEcrire.forEach(function (bouton) {
                bouton.disabled = true;
            });
            calculerVitesseDefilement();
            modeEcrire = false;
        } catch (error) {
            console.error('Erreur lors du nettoyage du texte :', error);
        }
    } else {
        alert('Pas de texte à afficher.');
    }
}


function calculerTempsDeLecture(){

}



// Calculer la vitesse de défilement d'après le nombre de mots par minute indiqué
async function calculerVitesseDefilement(){
    vitesseLecture = parseInt(inputVitesseLecture.value);
    stocke('vitesse-lecture',vitesseLecture);
    majUrlCle('vitesse-lecture',vitesseLecture);
    if (texte){

        console.log(texte);


        // Remplacer les balises <br> par des espaces
        texte = texte.replace(/<style[\s\S]*?>[\s\S]*?<\/style>/gi, '');

        // Remplacer les balises <br> par des espaces
        texte = texte.replace(/<br\s*\/?>/gi, ' ');
    
        let div = document.createElement("div");
        div.innerHTML = texte;
        let texteBrut = div.textContent || div.innerText || "";
    
        // Supprimer les espaces multiples et les réduire à un seul espace
        texteBrut = texteBrut.replace(/\s+/g, ' ').trim();
    
        console.log(texteBrut)

        // Calcul du nombre de mots
        // Suppression des espaces en début et fin de texte, puis séparation par les espaces
        const mots = texteBrut.trim().split(/\s+/);
        // Nombre de mots
        const nombreDeMots = mots.length;
        console.log('Nombre de mots = '+nombreDeMots);    
        // Temps de lecture
        const dureeLecture = Math.round( ( nombreDeMots / vitesseLecture ) * 60000);
        console.log('Temps de lecture = '+dureeLecture+ ' ms');
        let minutes = Math.floor(dureeLecture / 60000);
        let secondes = ((dureeLecture % 60000) / 1000).toFixed(0);
        console.log('Temps de lecture = ' + minutes + ' min ' + (secondes < 10 ? '0' : '') + secondes + ' sec');
        tempsLecture.innerHTML = 'Temps ' + minutes + ' min ' + (secondes < 10 ? '0' : '') + secondes + ' sec';

        let pixelsAScroller;

        console.log('mode '+modeDefilement);

        if (modeDefilement === 'vertical'){
            console.log('Hauteur scroll = '+divConteneurTexte.scrollHeight + ' px');
            console.log('Hauteur = '+divConteneurTexte.offsetHeight + ' px');
            pixelsAScroller = divConteneurTexte.scrollHeight - divConteneurTexte.offsetHeight;
            console.log('Hauteur à scroller = '+pixelsAScroller + ' px');
        }

        else {
            console.log('Largeur scroll = '+divConteneurTexte.scrollWidth + ' px');
            console.log('Largeur = '+divConteneurTexte.offsetWidth + ' px');
            pixelsAScroller = divConteneurTexte.scrollWidth - divConteneurTexte.offsetWidth + 200;
            console.log('Largeur à scroller = '+pixelsAScroller + ' px');
        }
        

        intervalle = Math.round(dureeLecture / pixelsAScroller);

        console.log('Intervalle scroll : 1px toutes les '+intervalle+' ms');

        if (lectureEnCours){  
            if (intervalle !=0){          
                clearInterval(defilementInterval);
                defilementInterval = setInterval(defilementAutomatique, intervalle);
            }
        }
    }
}

// Changement du thème
function changeClairSombre() {
    console.log('changement de mode de couleurs');
    let opacite = inputOpacite.value;
	if (modeCouleur == "light") {
        modeCouleur = "dark";
		boutonClairsombre.style.backgroundPosition = 'bottom';
        root.style.setProperty('--couleur-fond-bas', `rgba(255, 255, 255, ${opacite/100})`);
	} else {
        modeCouleur = "light";
		boutonClairsombre.style.backgroundPosition = null;
        root.style.setProperty('--couleur-fond-bas', `rgba(0, 0, 0, ${opacite/100})`);
	}
    console.log('mode '+modeCouleur);
	document.documentElement.setAttribute("data-theme", modeCouleur);
}

function changeOpacite(opacite,manuel){
    const currentTheme = root.getAttribute('data-theme');        
    if (currentTheme === 'dark') {
        root.style.setProperty('--couleur-fond-bas', `rgba(255, 255, 255, ${opacite/100})`);
    } else {
        root.style.setProperty('--couleur-fond-bas', `rgba(0, 0, 0, ${opacite/100})`);
    }
    if (manuel){
        stocke('opacite',opacite);
    }
}


// Changement horizontal / vertical
function changeModeDefilement() {
    console.log('Changement de mode');
    if (modeDefilement === "horizontal") {
        modeDefilement = "vertical";
        divConteneurTexte.classList.remove('horizontal');  
        boutonScroll.style.transform=null;      
    } else {
        modeDefilement = "horizontal";
        divConteneurTexte.classList.add('horizontal');
        boutonScroll.style.transform="rotate(90deg)";
    }
    if (!modeEcrire){
        texteDefilant.innerHTML = formateTexte(entreeTexte.innerHTML);
        calculerVitesseDefilement();
    }
}

function changeLargeurTexte(){
    let largeur = inputLargeur.value;
    if (window.innerWidth*largeur/100 > 300) {
        divConteneurTexte.style.width = 'calc('+largeur+'% - 200px)';
        divConteneurTexte.style.setProperty('--after-width', 'calc('+largeur+'% - 100px)');
    } else {
        divConteneurTexte.style.width = '100px';
        divConteneurTexte.style.setProperty('--after-width', '200px');
    }
}

// Play / Pause
function demarreDefilement() {
    calculerVitesseDefilement();    
    if (lectureEnCours){
        lectureEnCours = false;
        boutonPlay.style.backgroundPosition=null;
        clearInterval(defilementInterval);
    } else {
        lectureEnCours = true;
        boutonPlay.style.backgroundPosition='bottom'; 
        // Lancer le défilement automatique à intervalles réguliers
        defilementInterval = setInterval(defilementAutomatique, intervalle);
    }
}

// Stop
function resetDefilement(){
    console.log('reset défilement')
    if (lectureEnCours){
        demarreDefilement();
    }
    divConteneurTexte.scrollTop = 0;
    calculerVitesseDefilement();
}

// Fonction pour faire défiler le contenu
function defilementAutomatique() {
    // Déplacer le contenu
    if (modeDefilement === 'vertical') {
        divConteneurTexte.scrollTop += 1;
    }
    else {
        divConteneurTexte.scrollLeft += 1; 
    }
    // Si le défilement a atteint le bas du contenu, revenir au début
    if ((modeDefilement==='vertical'  && divConteneurTexte.scrollTop >= divConteneurTexte.scrollHeight - divConteneurTexte.clientHeight)||(modeDefilement==='horizontal'  && divConteneurTexte.scrollLeft >= divConteneurTexte.scrollWidth - divConteneurTexte.clientWidth)) {
        demarreDefilement();
    }
}


// Changer la taille de police
function tailleTexte(sens){
    taillePolice+= sens*4;
    divConteneurTexte.style.fontSize = taillePolice + 'px';
    entreeTexte.style.fontSize = taillePolice + 'px';
    calculerVitesseDefilement();
}

// Appuis clavier
document.addEventListener('keydown', function(event) {
    // Vérifier si la touche enfoncée est la barre d'espace (code 32)
    if (event.keyCode === 32 && !modeEcrire) {
        // Empêcher le défilement de la page lorsque la barre d'espace est enfoncée
        event.preventDefault();
        // Appeler la fonction demarreDefilement
        demarreDefilement();
    }
});


